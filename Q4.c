#include <stdio.h>
int main()
{
    float a, b;

    printf("Enter any temperature in Celsius: ");
    scanf("%f", &a);

    b = (a * 9 / 5) + 32;

    printf("%.2f Celsius = %.2f Fahrenheit", a, b);

    return 0;
}