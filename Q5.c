#include <stdio.h>
int main()
{
    int A = 10, B = 15;
    printf("Output A&B = %d\n", A&B);
    printf("Output A^B = %d\n", A^B);
    printf("Output ~A = %d\n", ~A);
    printf("Output A<<3 = %d\n", A<<3);
    printf("Output B>>3 = %d\n", B>>3);
return 0;
}